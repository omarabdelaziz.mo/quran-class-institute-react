import React from 'react';
import { Link } from 'react-router-dom';
import loginImg from '../../images/login.svg';
import './Login.css';
function LogIn(props){
    return (
        <div className="base-container">
            <div className="login-card">
            <h2 className="header">
                Login
            </h2>
            <div className="content">
                    <div className="image">
                        <img src={loginImg} alt="login" />
                    </div>
                    <form className="form">
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input type="text" name="username" placeholder="Username"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="password" name="password" placeholder="Password"/>
                        </div>
                        <div className="footer">
                            <button type="button" className="btn">
                                Login
                            </button>
                        </div>
                    </form>
                </div>

                <div className="sign-up-link">
                    <p>Don't have an account? <Link to="/sign-up">Sign</Link></p>
                </div>
            </div>
        </div>
    )
}
export default LogIn;