import React from 'react';
import { Link } from 'react-router-dom';
import loginImg from '../../images/login.svg';
import './Signup.css';
function SignUp(props){
    return (
        <div className="base-container">
            <div className="sign-up-card">
            <h2 className="header">
                Sign Up
            </h2>
            <div className="content">
                    <div className="image">
                        <img src={loginImg} alt="login" />
                    </div>
                    <form className="form">
                        <div className="form-group">
                            <label htmlFor="first-name">First Name</label>
                            <input type="text" name="first-name" placeholder="First Name"/>
                            <label htmlFor="last-name">Last Name</label>
                            <input type="text" name="last-name" placeholder="Last Name"/>
                        </div>
                        
                        <div className="form-group">
                            <label htmlFor="email">E-mail</label>
                            <input type="email" name="email" placeholder="E-Mail"/>
                        </div>
                        <div className="form-group">
                                <label htmlFor="type">Type</label>
                                <select id="type" name="type" size="1">
                                    <option className="select-items" value="student">Student</option>
                                    <option className="select-items" value="teaacher">Teacher</option>
                                </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="skype">Skype</label>
                            <input type="text" name="skype" placeholder="skype"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="address">Address</label>
                            <input type="text" name="address" placeholder="Address"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="country">Country</label>
                            <input type="text" name="country" placeholder="Country"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="age">Age</label>
                            <input type="number" name="age" placeholder="Age"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="password" name="password" placeholder="Password"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password2">Confirm Password</label>
                            <input type="password" name="password2" placeholder="Confirm Password"/>
                        </div>
                        <div className="footer">
                            <button type="button" className="btn">
                                Signup
                            </button>
                        </div>
                    </form>
                </div>

                <div className="sign-up-link">
                    <p>Don't have an account? <Link to="/sign-up">Sign</Link></p>
                </div>
            </div>
        </div>
    )
}
export default SignUp;