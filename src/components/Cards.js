import React from 'react';
import CardItem from './CardItem';
import './Cards.css';

const CONTAINER_STYLES = ['cards__container', 'main__container'];
const WRAPPER_STYLES = ['cards__items', 'grid__wrapper'];

function Cards(props){
    const checkContainerStyle = CONTAINER_STYLES.includes(props.containerStyle) ? props.containerStyle : CONTAINER_STYLES[0];

    const checkWrapperStyle = WRAPPER_STYLES.includes(props.wrapperStyle) ? props.wrapperStyle : WRAPPER_STYLES[0];
    return (
        <>
            <h1 className={props.display}>Check out this epic destination</h1>
            <div className={checkContainerStyle}>
                    <ul className={checkWrapperStyle}>
                        <CardItem CardItemStyle={props.CardItemStyle} src="../images/img-3.jpeg" text="Learn Tajweed online!" label="QURAN" path="/course-details"/>
                        <CardItem CardItemStyle={props.CardItemStyle} src="../images/img-2.jpeg" text="Quran Memorization!" label="QURAN" path="/course-details"/>
                        <CardItem CardItemStyle={props.CardItemStyle} src="../images/img-1.jpeg" text="Quran Recitation!" label="QURAN" path="/course-details"/>
                        <CardItem CardItemStyle={props.CardItemStyle} src="../images/img-1.jpeg" text="Quran Recitation!" label="QURAN" path="/course-details"/>
                        <CardItem CardItemStyle={props.CardItemStyle} src="../images/img-1.jpeg" text="Quran Recitation!" label="QURAN" path="/course-details"/>
            
                    </ul>
            </div>
        </>
    )

}

export default Cards;