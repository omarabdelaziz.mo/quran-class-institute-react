import React from 'react';
import { Link } from 'react-router-dom';
import './UsersCards.css';


function UsersCards(){

    return (
        <>
        <div className="profile-container">
            <div className="profile-card">
                <img src="https://i.imgur.com/bZBG9PE.jpg" alt="image1" className="profile-icon" />
                <div className="profile-name">Kelly Seikma</div>
                <div className="profile-position">Web Designer</div>
                <Link to="/teacher-details" className="button">View</Link>
            </div>
            <div className="profile-card">
                <img src="https://i.imgur.com/S4GNFIW.jpg" alt="image2" className="profile-icon" />
                <div className="profile-name">Mabel Maxwell</div>
                <div className="profile-position">Web Developer</div>
                <Link to="/teacher-details" className="button">View</Link>
            </div>
            <div className="profile-card">
                <img src="https://i.imgur.com/cMSVQZC.jpg" alt="image3" className="profile-icon" />
                <div className="profile-name">Danny Liswell</div>
                <div className="profile-position">DevOps</div>
                <Link to="/teacher-details" className="button">View</Link>
            </div>
            <div className="profile-card">
                <img src="https://i.imgur.com/CMddVCL.jpg" alt="image3" className="profile-icon" />
                <div className="profile-name">Danny Liswell</div>
                <div className="profile-position">DevOps</div>
                <Link to="/teacher-details" className="button">View</Link>
            </div>
        </div>
        </>
    )
}
export default UsersCards;