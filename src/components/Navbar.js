import React, {useState, useEffect} from 'react';
import { Link,useLocation } from 'react-router-dom';
import { Button } from './Button';
import './Navbar.css';
import Logo from '../images/logo.png'
import $ from 'jquery';

function Navbar(){
    const [click, setClick] = useState(false);
    const [button, setButton] = useState(true);

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    const showMobile = () => {
        if(window.innerWidth <= 960){
            setButton(false);
        }else{
            setButton(true);
        }
    }

    useEffect(()=>{
        setButton(true);
    },[]);

    window.addEventListener('resize', showMobile);
    var locate = useLocation();
    const handleScroll = ()=>{
        if(locate.pathname === '/'){
            $('html, body').animate({
                scrollTop: $( '#plans' ).offset().top
    
            }, 500);    
        }
                
    }
    return (
        <>
            <nav className="navbar">
                <div className="navbar-container">
                    <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
                        <img src={Logo} alt="website logo"/>
                    </Link>
                    <div className="menu-icon" onClick={handleClick}>
                        <i className={click ? 'fa fa-times' : 'fas fa-bars'}></i>
                    </div>
                    <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                        <li className="nav-item">
                            <Link to="/" className="nav-links" onClick={closeMobileMenu}>
                                Home
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/courses" className="nav-links" onClick={closeMobileMenu}>
                                Courses
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/teachers" className="nav-links" onClick={closeMobileMenu}>
                                Teachers
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/blogs" className="nav-links" onClick={closeMobileMenu}>Blogs</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/#plans" className="nav-links" onClick={()=>{
                                closeMobileMenu();
                                handleScroll();}}>Plans</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/login" className="nav-links" onClick={closeMobileMenu}>Login</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/sign-up" className="nav-links-mobile" onClick={closeMobileMenu}>
                                Sign Up
                            </Link>
                        </li>
                        
                    </ul>
                    {button && <Button buttonStyle="btn--outline">SIGN UP</Button>}
                </div>
            </nav>
        </>
    )
}

export default Navbar