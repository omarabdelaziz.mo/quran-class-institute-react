import React from 'react';
import '../../App.css';
import Cards from '../Cards';

function Courses(){
    return (
        <>
        <h1 className="courses">Courses</h1>
        <Cards CardItemStyle="cards__item__courses" display="d-none" containerStyle="main__container" wrapperStyle="grid__wrapper"/>
        </>
    )
}

export default Courses;