import React from 'react';
import '../App.css'
import { Button } from './Button';
import './HeroSection.css';
import img from '../images/img-1.jpeg';
function HeroSection(){
    return (
        <div className="hero-container">
            <img src={img} alt="slider"/>
            <h1>LET'S START</h1>
            <p>What are you waiting for?</p>
            <div className="hero-btns">
                <Button className="btns" buttonStyle="btn--outline" buttonSize="btn--large">GET STARTED</Button>
                <Button className="btns" buttonStyle="btn--primary" buttonSize="btn--large">2 FREE TRIALS</Button>

            </div>
        </div>
    )
}

export default HeroSection;