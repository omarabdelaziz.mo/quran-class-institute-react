import React, { useEffect } from 'react';
import '../../App.css';
import HeroSection from '../HeroSection';
import Cards from '../Cards';
import PricingCard from '../PricingCard';
function Home(){
    useEffect(()=>{
        const prevTitle = "Quran Class Institute"
        return () => {
        document.title = prevTitle
        }
     });
    return (
        <>
        <HeroSection />
        <div className="courses_container">
            <Cards />
        </div>
        <div className="plans_container" id="plans">
            <h1>Checkout our plans</h1>
            <PricingCard />
        </div>
        </>
    )
}

export default Home;