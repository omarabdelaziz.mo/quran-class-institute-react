import React  from 'react';
import './App.css';
import Navbar from './components/Navbar';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Home from './components/pages/Home';
import Courses from './components/pages/Courses';
import Footer from './components/Footer';
import SignUp from './components/pages/SignUp';
import LogIn from './components/pages/LogIn';
import Teachers from './components/pages/Teachers';
import TeacherDetails from './components/pages/TeacherDetails';
import CourseDetailsPage from './components/pages/CourseDetailsPage';
import BlogsPage from './components/pages/BlogsPage';
import BlogsDetails from './components/pages/BlogsDetails';
function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/courses" component={Courses}/>
          <Route path="/teachers" component={Teachers}/>
          <Route path="/login" component={LogIn} />
          <Route path="/sign-up" component={SignUp}/>
          <Route path="/teacher-details" component={TeacherDetails}/>
          <Route path="/course-details" component={CourseDetailsPage} />
          <Route path="/blogs" component={BlogsPage} />
          <Route path="/blog-details" component={BlogsDetails} />
        </Switch>
        <Footer/>
      </Router>
    </>
  );
}

export default App;
  