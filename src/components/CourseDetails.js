import React from 'react';
import './CourseDetails.css'
function CourseDetails(){
    return (
        <>
        
        <div className="content__wrapper">
            <div className="main__container">
            <div className="main__image">
                <img src="https://images.unsplash.com/photo-1560419284-6c2d2b5e0483?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1234&q=80" alt="gamer-bro" width="300px" height="auto"/>
            </div>
            <article className="main__text">
                <header>
                <h1>
                    Learning Html and CSS
                </h1>
                </header>
                <p>Occaecat elit laborum quis deserunt ullamco Lorem pariatur esse aliqua eiusmod quis deserunt. Anim excepteur velit fugiat Lorem labore. Eu laborum pariatur magna mollit pariatur id consectetur non nostrud enim. Incididunt est occaecat anim et mollit occaecat duis commodo in. Dolor sunt ut incididunt dolore aliquip magna sint minim Lorem.</p>
                <button className="btn-enroll">Enroll</button>
            </article>
            
            </div>
        </div>
        </>
    )
}

export default CourseDetails;