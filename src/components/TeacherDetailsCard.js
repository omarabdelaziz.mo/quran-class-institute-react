import React from 'react';
import './TeacherDetailsCard.css';

function TeacherDetailsCard(){
    return (
    <>
        <main className="main">
            <div className="container">
                <div className="inner">
                    <div className="inner__headings">
                        {/* <h2 className="inner__head">Having attractive
                        services has never
                        been <span className="inner__clr">easier.</span></h2> */}
                        <img className="circ1 inner__head" src="https://i.imgur.com/bZBG9PE.jpg" alt="teacher details" />
                    </div>
                    <div className="inner__content">
                        <h5 className="inner__sub">Our Capabilities</h5>
                        <p className="inner__text">We're brand strategy and digital design agency, building brands that
                        matter in culture with more than ten years of knowledge e and in to do expertise and digital design agency or brands.</p>
                    </div>
                </div>
                <div className="cards-grid">
                    <div className="card">
                        <div className="card__body">
                        <h4 className="card__head">UI/UX Design</h4>
                        <p className="card__content">Landing Pages, User Flow, Wireframing, Prototyping, Mobile App Design, Web App</p>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card__body">
                        <h4 className="card__head">UI/UX Design</h4>
                        <p className="card__content">Landing Pages, User Flow, Wireframing, Prototyping, Mobile App Design, Web App</p>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card__body">
                        <h4 className="card__head">UI/UX Design</h4>
                        <p className="card__content">Landing Pages, User Flow, Wireframing, Prototyping, Mobile App Design, Web App</p>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </>
    )
}

export default TeacherDetailsCard;