import React from 'react'
import TeacherDetailsCard from '../TeacherDetailsCard';
import '../../App.css'

function TeacherDetails(){
    
    return (
        <>
        <div className="teacher-details-container">
            <h2 className="teachers-head">Teacher Details</h2>
            <TeacherDetailsCard />
        </div>
        </>
    )
}
 export default TeacherDetails;