import React from 'react';
import { Link } from 'react-router-dom';

const ITEM_STYLES = ['cards__item', 'cards__item__courses'];

function CardItem(props){
    const checkCardItemStyle = ITEM_STYLES.includes(props.CardItemStyle) ? props.CardItemStyle : ITEM_STYLES[0];

    return (
        <>
            <li className={checkCardItemStyle}>
                <Link className='cards__item__link' to={props.path}>
                <figure className='cards__item__pic-wrap' data-category={props.label}>
                    <img
                    className='cards__item__img'
                    alt='Travel '
                    src={props.src}
                    />
                </figure>
                <div className='cards__item__info'>
                    <h5 className='cards__item__text'>{props.text}</h5>
                </div>
                </Link>
      </li>
    </>
    )
}

export default CardItem;