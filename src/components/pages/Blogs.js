import React from 'react';
import { Link } from 'react-router-dom';
import './Blogs.css'
function Blogs(){
    return (
        <>
        <Link className="blog-details-link" to="/blog-details">
            <div className="blog-card">
                <div className="blog-header">
                    <h2 className="blog-blogcategory">Travel</h2>
                </div>

                <div className="blog-content">
                    <div className="blog-picture-wrapper"></div>
                        
                    <div className="blog-text-wrapper">  
                        <h1 className="blog-title">10 great things to do in Buenos Aires</h1>
                        <h3 className="blog-author">By Natalia Lavega</h3>
                        <p className="blog-text">
                            The beginning of spring is one of the best moments to 
                            visit the city. Let's explore...
                        </p>
                    </div>
                </div>
            </div>
        </Link>
        </>
    )
}

export default Blogs;