import React from 'react';
import UsersCards from '../UsersCards';
import '../../App.css'
function Teachers(){
    
    return (
        <>
            <h2 className="teachers-head">Our Teachers</h2>
            <UsersCards />
        </>
    )
}

export default Teachers;