import React from 'react';
import './BlogsDetails.css'
function BlogsDetails(){
    
    return (
        <>
    
            <div class="blog-details-container">
                <h2>Lorem Ipsum Dolor Sit Amet</h2>

                <img alt="Gambar" src="https://1.bp.blogspot.com/-JYkKYhlA5F8/YAkNXyQ28aI/AAAAAAAADng/qnBjTkWKiWY_cAhd8ESWcezRJJUI5x40ACLcBGAsYHQ/s0/test.png" title="Gambar" />

                <p><b>Lorem Ipsum Dolor Sit Amet</b> - Accusamus officiis aperiam sequi suscipit explicabo itaque nihil amet. Vero quam possimus, iure labore dolorem ratione consequuntur soluta sed delectus quibusdam? Quaerat. Lorem ipsum dolor, sit amet consectetur adipisicing elit. A distinctio saepe reprehenderit ipsam ab, natus ipsum fuga enim similique ratione consequuntur consectetur facilis itaque magnam iusto sequi delectus error suscipit.</p>

                <p>Tenetur accusantium atque dicta nemo. Cum debitis repellat, praesentium facilis, dolore totam voluptates odio vitae ab eius itaque ex quisquam, beatae est! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi optio voluptates delectus doloremque est tenetur inventore. Libero ex ipsum at architecto laboriosam, provident doloremque consequuntur fugiat quis, placeat odio cupiditate.</p>

                <p>Minima quia optio veniam molestiae voluptate excepturi expedita similique tempore facilis debitis quis exercitationem explicabo, dolor maiores culpa cum nemo laboriosam ratione? Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo fuga nemo aperiam minima laudantium facere laborum mollitia explicabo ex molestiae quasi aut placeat esse voluptatibus libero, accusantium labore, obcaecati sed!</p>

                <p>Nostrum quae dicta sit minima pariatur quam nam, ipsum possimus illum consequuntur sunt fugiat ex iusto? Deleniti libero quo cumque numquam mollitia? Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe, magnam officia vitae unde rerum sed itaque ipsum voluptatum nostrum eveniet hic blanditiis deserunt reprehenderit quas accusamus, laboriosam perspiciatis sapiente cumque.</p>

                <p>Nulla eum harum consequatur dolorum consequuntur, accusantium debitis porro autem cum qui commodi quia exercitationem iste id incidunt ea asperiores nostrum eius! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deleniti cumque praesentium nam obcaecati molestias quisquam nihil? Modi accusamus consectetur nihil, eligendi similique quo nam id? Sunt officiis ratione eaque ullam.</p>
                <p></p>
        </div>
        </>
    )
}
export default BlogsDetails;