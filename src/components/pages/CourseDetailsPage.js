import React, { useEffect } from 'react';
import CourseDetails from '../CourseDetails';

function CourseDetailsPage(){
    useEffect(()=>{
        const prevTitle = "Course Details"
        return () => {
        document.title = prevTitle
        }
     });
    return (
        <>
            <CourseDetails />
        </>
    )
}

export default CourseDetailsPage;