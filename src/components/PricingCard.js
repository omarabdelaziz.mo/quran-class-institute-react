import React from 'react';
import { Link } from 'react-router-dom';
import './PricingCard.css';
function PricingCard(){
    return (
        <>
            <div className="pricing__container">
                <section className="pricing__card">
                    <div className="pricing__icon"><i className="fa fa-paper-plane" aria-hidden="true"></i></div>
                    <h3>Basic</h3>
                    <h4><sup>$</sup>25</h4>
                    <ul>
                        <li><i className="fa fa-check" aria-hidden="true"></i> 10Gb Space</li>
                        <li><i className="fa fa-check" aria-hidden="true"></i> 5 Domain Names</li>
                        <li><i className="fa fa-check" aria-hidden="true"></i> 2 E-mail Address</li>
                        <li><i className="fa fa-times" aria-hidden="true"></i> Live Support</li>
                    </ul>
                    <Link to="#" className="pricing__btn">Subscribe</Link>
                </section>
                <section className="pricing__card">
                    <div className="pricing__icon standard"><i className="fa fa-plane" aria-hidden="true"></i></div>
                    <h3>Standard</h3>
                    <h4><sup>$</sup>50</h4>
                    <ul>
                        <li><i className="fa fa-check" aria-hidden="true"></i> 20Gb Space</li>
                        <li><i className="fa fa-check" aria-hidden="true"></i> 5 Domain Names</li>
                        <li><i className="fa fa-check" aria-hidden="true"></i> 2 E-mail Address</li>
                        <li><i className="fa fa-times" aria-hidden="true"></i> Live Support</li>
                    </ul>
                    <Link to="#" className="pricing__btn">Subscribe</Link>
                </section>
                <section className="pricing__card">
                    <div className="pricing__icon premium"><i className="fa fa-rocket" aria-hidden="true"></i></div>
                    <h3>Premium</h3>
                    <h4><sup>$</sup>100</h4>
                    <ul>
                        <li><i className="fa fa-check" aria-hidden="true"></i> 50Gb Space</li>
                        <li><i className="fa fa-check" aria-hidden="true"></i> 10 Domain Names</li>
                        <li><i className="fa fa-check" aria-hidden="true"></i> 5 E-mail Address</li>
                        <li><i className="fa fa-check" aria-hidden="true"></i> Live Support</li>
                    </ul>
                    <Link to="#" className="pricing__btn">Subscribe</Link>
                </section>
            </div>
        </>
    )
}

export default PricingCard;